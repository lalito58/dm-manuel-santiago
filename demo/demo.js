import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import '@cells-components/coronita-icons/coronita-icons.js';
import './css/demo-styles.js';
import '../dm-manuel-santiago.js';
import './login-fake';
import '@bbva-web-components/bbva-button-default';

// Include below here your components only for demo
// import 'other-component.js'