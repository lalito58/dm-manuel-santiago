import { LitElement, html } from 'lit-element';
import { BGADPGrantingTicketsPOST } from '@cells-components/bgadp-granting-tickets';
/**
 * `LowerCaseDashedName` Description
 * @customElement
 * @polymer
 * @demo
 */
class LoginFake extends LitElement {
  static get properties() {
    return {
      host: { type: String, },
    };
  }
  /**
   * Implement to describe the element's DOM using lit-html.
   * Use the element current props to return a lit-html template result
   * to render into the element.
   */
  render() {
    return html``;
  }
  /**
  * Instance of the element is created/upgraded. Use: initializing state,
  * set up event listeners, create shadow dom.
  * @constructor
  */
  constructor() {
    super();
  }
  firstUpdated() {
    this.fakeLogin();
  }
  fakeLogin() {
    const sessionDemo = new BGADPGrantingTicketsPOST('mx', {
      host: this.host,
      version: 0
    });
    sessionDemo.generateRequest(true, {
      userId: '1234567890',
      password: '112233',
      consumerId: '10000033'
    }).then(response => {
      this.dispatchEvent(new CustomEvent('fake-login', {
        bubbles: true,
        composed: true,
        detail: response
      }));
    });
  }
}
customElements.define('login-fake', LoginFake);