import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import '../dm-manuel-santiago.js';

suite('<dm-manuel-santiago>', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<dm-manuel-santiago></dm-manuel-santiago>`);
    await el.updateComplete;
  });

  test('instantiating the element with default properties works', () => {
    const element = el.shadowRoot.querySelector('p');
    assert.equal(element.innerText, 'Welcome to Cells');
  });

});





