import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './dm-manuel-santiago-styles.js';
import { BGADPCardsGetV0 } from '@cells-components/bgadp-cards-v0';

/**
This component ...

Example:

```html
<dm-manuel-santiago></dm-manuel-santiago>
```

##styling-doc

@customElement dm-manuel-santiago
@polymer
@LitElement
@demo demo/index.html
*/
export class DmManuelSantiago extends LitElement {
  static get is() {
    return 'dm-manuel-santiago';
  }

  // Declare properties
  static get properties() {
    return {
      host: { type: String, },
      version: { type: Number, },
      requierdToken: { type: String, },
      native: { type: Boolean, },
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.host = '';
    this.version = 0;
    this.requiredToken = 'tsec';
    this.native = false;
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('dm-manuel-santiago-shared-styles')
    ];
  }

  // Define a template
  render() {
    return html``;
  }

  // Get cards
  getCards() {
    const service = new BGADPCardsGetV0({
      host: this.host,
      version: this.version,
      requiredToken: this.requiredToken,
      native: this.native
    });
    service.generateRequest().then(response => {
      response = JSON.parse(response.response);
      this.dispatchEvent(new CustomEvent('cards-list', {
        bubbles: true,
        composed: true,
        detail: response.data
      }));
    }).catch(error => {
      this.dispatchEvent(new CustomEvent('request-error', {
        bubbles: true,
        composed: true,
        detail: JSON.parse(error)
      }));
    });
  }
}

// Register the element with the browser
customElements.define(DmManuelSantiago.is, DmManuelSantiago);
